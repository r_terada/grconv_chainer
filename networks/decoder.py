# -*- coding: utf-8 -*-
from __future__ import print_function, division

from util.XP import XP
from chainer import Chain
from chainer import functions as F
from chainer import links as L
from chainer import cuda


class Decoder(Chain):
    def set_h(self, h):
        self.h = h

    def get_h(self):
        return self.h

    def reset(self, batch_size):
        self.batch_size = batch_size
        self.c = XP.fzeros((batch_size, self.hidden_size))
        self.h = XP.fzeros((batch_size, self.hidden_size))

    # abstract method
    def forward():
        raise NotImplementedError()


class LSTM(Decoder):
    def __init__(self, vocab_size, embed_size, hidden_size):
        super(Decoder, self).__init__(
            ye=L.EmbedID(vocab_size, embed_size, ignore_label=-1),  # -1 indicates <ignore>
            eh=L.Linear(embed_size, 4 * hidden_size),
            hh=L.Linear(hidden_size, 4 * hidden_size),
            hf=L.Linear(hidden_size, embed_size),
            fy=L.Linear(embed_size, vocab_size),
        )
        self.vocab_size = vocab_size
        self.embed_size = embed_size
        self.hidden_size = hidden_size

    def forward_onestep(self, y):
        """
        y: word_id
        c: inner_state of lstm
        h: hidden units
        """
        e = F.tanh(self.ye(y))
        self.c, self.h = F.lstm(self.c, self.eh(e) + self.hh(self.h))
        f = F.tanh(self.hf(self.h))
        return self.fy(f)

    def forward(self, y_batch, vocab, generation_limit=30, is_training=True):
        sentence_len = len(y_batch[0]) if y_batch else 0
        t = XP.iarray([vocab.stoi('<s>') for _ in xrange(self.batch_size)])
        hyp_batch = [[] for _ in xrange(self.batch_size)]

        if is_training:
            loss = XP.fzeros(())
            for l in xrange(sentence_len):
                y = self.forward_onestep(t)
                t = XP.iarray([vocab.stoi(y_batch[k][l]) for k in xrange(self.batch_size)])
                loss += F.softmax_cross_entropy(y, t)
                output = cuda.to_cpu(y.data.argmax(1))
                for k in xrange(self.batch_size):
                    hyp_batch[k].append(vocab.itos(output[k]))

            return hyp_batch, loss

        else:
            while len(hyp_batch[0]) < generation_limit:
                y = self.forward_onestep(t)
                output = cuda.to_cpu(y.data.argmax(1))
                t = XP.iarray(output)
                for k in xrange(self.batch_size):
                    hyp_batch[k].append(vocab.itos(output[k]))
                if all(hyp_batch[k][-1] == '</s>' for k in xrange(self.batch_size)):
                    break

            return hyp_batch

    def __call__(self, y_batch, vocab, generation_limit=30, is_training=True):
        return self.forward(y_batch, vocab, generation_limit, is_training)


class ResidualLSTM(Decoder):
    def __init__(self, vocab_size, embed_size, hidden_size):
        super(Decoder, self).__init__(
            ye=L.EmbedID(vocab_size, embed_size, ignore_label=-1),  # -1 indicates <ignore>
            eh=L.Linear(embed_size, 4 * hidden_size),
            hh=L.Linear(hidden_size, 4 * hidden_size),
            hf=L.Linear(hidden_size, embed_size),
            fy=L.Linear(embed_size, vocab_size),
        )
        self.vocab_size = vocab_size
        self.embed_size = embed_size
        self.hidden_size = hidden_size

    def forward_onestep(self, y):
        """
        y: word_id
        c: inner_state of lstm
        h: hidden units
        """
        e = F.tanh(self.ye(y))
        self.c, self.h = F.lstm(self.c, self.eh(e) + self.hh(self.h))
        f = F.tanh(self.hf(self.h)) + self.h
        return self.fy(f)

    def forward(self, y_batch, vocab, generation_limit=30, is_training=True):
        sentence_len = len(y_batch[0]) if y_batch else 0
        t = XP.iarray([vocab.stoi('<s>') for _ in xrange(self.batch_size)])
        hyp_batch = [[] for _ in xrange(self.batch_size)]

        if is_training:
            loss = XP.fzeros(())
            for l in xrange(sentence_len):
                y = self.forward_onestep(t)
                t = XP.iarray([vocab.stoi(y_batch[k][l]) for k in xrange(self.batch_size)])
                loss += F.softmax_cross_entropy(y, t)
                output = cuda.to_cpu(y.data.argmax(1))
                for k in xrange(self.batch_size):
                    hyp_batch[k].append(vocab.itos(output[k]))

            return hyp_batch, loss

        else:
            while len(hyp_batch[0]) < generation_limit:
                y = self.forward_onestep(t)
                output = cuda.to_cpu(y.data.argmax(1))
                t = XP.iarray(output)
                for k in xrange(self.batch_size):
                    hyp_batch[k].append(vocab.itos(output[k]))
                if all(hyp_batch[k][-1] == '</s>' for k in xrange(self.batch_size)):
                    break

            return hyp_batch

    def __call__(self, y_batch, vocab, generation_limit=30, is_training=True):
        return self.forward(y_batch, vocab, generation_limit, is_training)
