# -*- coding: utf-8 -*-
from __future__ import print_function, division

from util.XP import XP
from chainer import Chain
from chainer import functions as F
from chainer import links as L


class Encoder(Chain):
    def set_h(self, h):
        self.h = h

    def get_h(self):
        return self.h

    def reset(self, batch_size):
        self.batch_size = batch_size

    # abstract method
    def forward():
        raise NotImplementedError()


class grConv(Encoder):
    def __init__(self, vocab_size, embed_size, hidden_size):
        super(Encoder, self).__init__(
            embed=L.EmbedID(vocab_size, embed_size),
            e_h=L.Linear(embed_size, hidden_size),
            W_l=L.Linear(hidden_size, hidden_size),
            W_r=L.Linear(hidden_size, hidden_size),
            G_l=L.Linear(hidden_size, 3),
            G_r=L.Linear(hidden_size, 3),
        )

    def make_h_0(self, x_list):
        """
        x_list: list of word_id(1, batch_size)
        """
        h_0_list = []
        for x in x_list:
            embedding = F.tanh(self.embed(x))
            h_0_list.append(F.tanh(self.e_h(embedding)))
        return h_0_list

    def forward_onestep(self, h_j_1, h_j):
        # w is 3-dimensional gating coefficients
        w = F.softmax(self.G_l(h_j_1) + self.G_r(h_j))
        h_j_bar = F.tanh(self.W_l(h_j_1) + self.W_r(h_j))
        size = h_j_bar.data.shape
        gate = {}
        gate["c"], gate["l"], gate["r"] = F.split_axis(w, 3, 1)
        h_j_new = F.tanh(F.reshape(F.batch_matmul(h_j_bar, gate["c"]) +
                         F.batch_matmul(h_j_1, gate["l"]) +
                         F.batch_matmul(h_j, gate["r"]), size))
        return h_j_new, gate

    def forward_onelayer(self, h_list):
        new_h_list = []
        for j in xrange(1, len(h_list)):
            h_j_new, _gate = self.forward_onestep(h_list[j-1], h_list[j])
            new_h_list.append(h_j_new)

        return new_h_list

    def forward(self, x_batch, vocab):
        x_list = []
        for l in xrange(len(x_batch[0])):
            x_list.append(XP.iarray([vocab.stoi(x_batch[k][l]) for k in xrange(self.batch_size)]))

        h_list = self.make_h_0(x_list)

        num_h = len(h_list)
        while num_h > 1:
            h_list = self.forward_onelayer(h_list)
            num_h = len(h_list)

        return h_list[0]

    def __call__(self, x_batch, vocab):
        self.h = self.forward(x_batch, vocab)


class grConvNormalizeInputgate(Encoder):
    def __init__(self, vocab_size, embed_size, hidden_size):
        super(Encoder, self).__init__(
            embed=L.EmbedID(vocab_size, embed_size),
            e_h=L.Linear(embed_size, hidden_size),
            W_l=L.Linear(hidden_size, hidden_size),
            W_r=L.Linear(hidden_size, hidden_size),
            G_l=L.Linear(hidden_size, 4),
            G_c=L.Linear(hidden_size, 4),
            G_r=L.Linear(hidden_size, 4),
        )
        self.vocab_size = vocab_size
        self.embed_size = embed_size
        self.hidden_size = hidden_size

    def make_h_0(self, x_list):
        """
        x_list: list of word_id [shape:(batch_size, 1)]
        """
        dummy_node = XP.fzeros((self.batch_size, self.hidden_size))

        h_0_list = []
        h_0_list.append(dummy_node)
        for x in x_list:
            embedding = F.tanh(self.embed(x))
            h_0_list.append(F.tanh(self.e_h(embedding)))
        h_0_list.append(dummy_node)

        return h_0_list

    def gating_coefficients(self, h_j_l, h_j_c, h_j_r):
        w = F.softmax(self.G_l(h_j_l) + self.G_c(h_j_c) + self.G_r(h_j_r))
        gate = {}
        gate["lt"], gate["lh"], gate["rh"], gate["rt"] = F.split_axis(w, 4, 1)

        return gate

    def forward_onestep(self, h_j_c):
        h_lt = F.tanh(self.W_l(h_j_c))
        h_rt = F.tanh(self.W_r(h_j_c))
        return h_lt, h_rt

    def forward_onelayer(self, h_list):
        h_shape = h_list[0].data.shape

        h_r = XP.fzeros(h_shape)
        new_h_list = []
        for j in xrange(1, len(h_list)-1):
            h_lt, h_rt = self.forward_onestep(h_list[j])
            gate = self.gating_coefficients(h_list[j-1], h_list[j], h_list[j+1])

            h_l = h_r + F.tanh(F.reshape(
                               F.batch_matmul(h_lt, gate["lt"]) +
                               F.batch_matmul(h_list[j], gate["lh"]),
                               h_shape))

            h_r = F.tanh(F.reshape(
                         F.batch_matmul(h_rt, gate["rt"]) +
                         F.batch_matmul(h_list[j], gate["rh"]),
                         h_shape))

            new_h_list.append(h_l)

        new_h_list.append(h_r)

        return new_h_list

    def forward(self, x_batch, vocab):
        x_list = []
        for l in xrange(len(x_batch[0])):
            x_list.append(XP.iarray([vocab.stoi(x_batch[k][l]) for k in xrange(self.batch_size)]))

        h_list = self.make_h_0(x_list)

        num_h = len(h_list)
        while num_h > 3:  # 3 = 1 + 2: 2 is number of dummy nodes
            h_list = self.forward_onelayer(h_list)
            num_h = len(h_list)

        return h_list[1]

    def __call__(self, x_batch, vocab):
        self.h = self.forward(x_batch, vocab)
