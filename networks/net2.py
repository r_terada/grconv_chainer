from __future__ import print_function, division

from util.XP import XP
from util.functions import trace
from util.graph_writer import set_initial_graph, ret_color
from chainer import Chain
from chainer import functions as F
from chainer import links as L
from chainer import cuda

import pydot


class grConv_inputnorm(Chain):
    def __init__(self, n_unit, activation=F.tanh):
        super(grConv_inputnorm, self).__init__(
            W_l=L.Linear(n_unit, n_unit),
            W_r=L.Linear(n_unit, n_unit),
            G_l=L.Linear(n_unit, 4),
            G_c=L.Linear(n_unit, 4),
            G_r=L.Linear(n_unit, 4),
        )
        self.activation = activation

    def __call__(self, h_j_l, h_j_c, h_j_r):
        # w is 3-dimensional gating coefficients
        w = F.softmax(self.G_l(h_j_l) + self.G_c(h_j_c) + self.G_r(h_j_r))
        h_lt = self.activation(self.W_l(h_j_c))
        h_rt = self.activation(self.W_r(h_j_c))
        gate = {}
        gate["lt"], gate["lh"], gate["rh"], gate["rt"] = F.split_axis(w, 4, 1)

        return h_lt, h_rt, gate


class SequenceDecoder(Chain):
    def __init__(self, vocab_size, embed_size, hidden_size):
        super(SequenceDecoder, self).__init__(
            ye=L.EmbedID(vocab_size, embed_size, ignore_label=2),  # 2 indicates </s>
            eh=L.Linear(embed_size, 4 * hidden_size),
            hh=L.Linear(hidden_size, 4 * hidden_size),
            hf=L.Linear(hidden_size, embed_size),
            fy=L.Linear(embed_size, vocab_size),
        )

    def __call__(self, y, c, h):
        e = F.tanh(self.ye(y))
        c, h = F.lstm(c, self.eh(e) + self.hh(h))
        f = F.tanh(self.hf(h))
        return self.fy(f), c, h


def grConv_factory(encoder_type):
    if encoder_type == "grConv":
        return grConv_inputnorm
    else:
        import sys
        print("encoder_type is invalid.", file=sys.stderr)
        sys.exit()


class grConvEncoderDecoder(Chain):
    def __init__(self, src_vocab_size, trg_vocab_size, embed_size, hidden_size, encoder_type="grConv"):
        super(grConvEncoderDecoder, self).__init__(
            embed=L.EmbedID(src_vocab_size, hidden_size),
            enc=grConv_factory(encoder_type)(hidden_size),
            dec=SequenceDecoder(trg_vocab_size, embed_size, hidden_size)
        )
        self.src_vocab_size = src_vocab_size
        self.trg_vocab_size = trg_vocab_size
        self.embed_size = embed_size
        self.hidden_size = hidden_size
        self.encoder_type = encoder_type

    def reset(self, batch_size):
        self.zerograds()
        self.c = XP.fzeros((batch_size, self.hidden_size))
        self.h = XP.fzeros((batch_size, self.hidden_size))

    def set_h(self, h):
        self.h = h

    def make_h_0(self, x):
        return F.tanh(self.embed(x))

    def encode(self, h_j_l, h_j_c, h_j_r):
        return self.enc(h_j_l, h_j_c, h_j_r)

    def decode(self, y):
        y, self.c, self.h = self.dec(y, self.c, self.h)
        return y

    def save_spec(self, filename):
        with open(filename, 'w') as fp:
            print(self.src_vocab_size, file=fp)
            print(self.trg_vocab_size, file=fp)
            print(self.embed_size, file=fp)
            print(self.hidden_size, file=fp)
            print(self.encoder_type, file=fp)

    @staticmethod
    def load_spec(filename):
        with open(filename) as fp:
            src_vocab_size = int(next(fp))
            trg_vocab_size = int(next(fp))
            embed_size = int(next(fp))
            hidden_size = int(next(fp))
            try:
                encoder_type = str(next(fp).strip())
            except:
                encoder_type = "grConv"

            return grConvEncoderDecoder(src_vocab_size, trg_vocab_size, embed_size, hidden_size, encoder=encoder_type)


def forward_train(src_batch, trg_batch, src_vocab, trg_vocab, encdec, generation_limit=30, prev_groundtruth=True):
    batch_size = len(src_batch)
    src_len = len(src_batch[0])
    trg_len = len(trg_batch[0]) if trg_batch else 0
    src_stoi = src_vocab.stoi
    trg_stoi = trg_vocab.stoi
    trg_itos = trg_vocab.itos
    encdec.reset(batch_size)

    dummy_node = XP.fzeros((batch_size, encdec.hidden_size))
    # encode
    h = []
    h.append(dummy_node)
    for l in xrange(src_len):
        x = XP.iarray([src_stoi(src_batch[k][l]) for k in xrange(batch_size)])
        h.append(encdec.make_h_0(x))
    h.append(dummy_node)

    cur_layer = src_len
    while cur_layer > 1:
        new_h = []
        new_h.append(dummy_node)
        for i in xrange(1, cur_layer+1):
            h_lt, h_rt, gate = encdec.encode(h[i-1], h[i], h[i+1])
            if i == 1:
                h_r = F.reshape(F.batch_matmul(h_rt, gate["rt"]) +
                                F.batch_matmul(h[i], gate["rh"]), h[i].data.shape)
            else:
                h_l = h_r + F.reshape(F.batch_matmul(h_lt, gate["lt"]) +
                                      F.batch_matmul(h[i], gate["lh"]), h[i].data.shape)
                h_r = F.reshape(F.batch_matmul(h_rt, gate["rt"]) +
                                F.batch_matmul(h[i], gate["rh"]), h[i].data.shape)
                new_h.append(encdec.enc.activation(h_l))
        new_h.append(dummy_node)
        h = new_h
        cur_layer -= 1

    h = [h[1]]
    if len(h) > 0:
        encdec.set_h(h[0])

    # decode
    t = XP.iarray([trg_stoi('<s>') for _ in xrange(batch_size)])
    hyp_batch = [[] for _ in xrange(batch_size)]

    if prev_groundtruth:
        loss = XP.fzeros(())
        for l in xrange(trg_len):
            y = encdec.decode(t)
            t = XP.iarray([trg_stoi(trg_batch[k][l]) for k in xrange(batch_size)])
            loss += F.softmax_cross_entropy(y, t)
            output = cuda.to_cpu(y.data.argmax(1))
            for k in xrange(batch_size):
                hyp_batch[k].append(trg_itos(output[k]))
        print(loss.data)
        # for k in xrange(batch_size):
            # print("src:", end=" ")
            # print([str(src_batch[k][l]) for l in xrange(src_len)])
            # print("trg:", end=" ")
            # print([str(trg_batch[k][l]) for l in xrange(trg_len)])
        return hyp_batch, loss

    else:
        while len(hyp_batch[0]) < generation_limit:
            y = encdec.decode(t)
            output = cuda.to_cpu(y.data.argmax(1))
            t = XP.iarray(output)
            for k in xrange(batch_size):
                hyp_batch[k].append(trg_itos(output[k]))
            if all(hyp_batch[k][-1] == '</s>' for k in xrange(batch_size)):
                break

        return hyp_batch


def forward_test(src_batch, trg_batch, src_vocab, trg_vocab, encdec, generation_limit, write_graph):
    batch_size = len(src_batch)
    src_len = len(src_batch[0])
    trace("batchsize: " + str(batch_size) + ", " + "word_num: " + str(src_len))
    src_stoi = src_vocab.stoi
    trg_stoi = trg_vocab.stoi
    trg_itos = trg_vocab.itos
    encdec.reset(batch_size)

    dummy_node = XP.fzeros((batch_size, encdec.hidden_size))

    if write_graph:
        graph, node_dic = set_initial_graph(src_batch[0], src_stoi)

        h = []
        for l in xrange(src_len):
            x = XP.iarray([src_stoi(src_batch[k][l]) for k in xrange(batch_size)])
            h.append(encdec.make_h_0(x))

        cur_layer = src_len
        while cur_layer > 1:
            new_h = []
            new_h.append(dummy_node)
            for i in xrange(1, cur_layer):
                h_lt, h_rt, gate = encdec.encode(h[i-1], h[i], h[i+1])
                if i == 1:
                    # h_l = encdec.activation(F.reshape(F.batch_matmul(h_lt, gate["lt"]) +
                                                      # F.batch_matmul(h[i], gate["l"]), h[i].data.shape))
                    h_r = F.reshape(F.batch_matmul(h_rt, gate["rt"]) +
                                    F.batch_matmul(h[i], gate["rh"]), h[i].data.shape)

                    edge_rh = pydot.Edge(node_dic["%d_%d" % (src_len-cur_layer+1, i+1)],
                                         node_dic["%d_%d" % (src_len-cur_layer+2, i)],
                                         # label="")
                                         label="%1.2f" % gate["rh"].data)
                    edge_rh.set_color(ret_color(gate["rh"].data))
                    graph.add_edge(edge_rh)
                    edge_rt = pydot.Edge(node_dic["%d_%d" % (src_len-cur_layer+1, i+1)],
                                         node_dic["%d_%d/%d" % (src_len-cur_layer+2, i, i+1)],
                                         # label="")
                                         label="%1.2f" % gate["rt"].data)
                    edge_rt.set_color(ret_color(gate["rt"].data))
                    graph.add_edge(edge_rt)

                else:
                    h_l = h_r + F.reshape(F.batch_matmul(h_lt, gate["lt"]) +
                                          F.batch_matmul(h[i], gate["l"]), h[i].data.shape)
                    h_r = F.reshape(F.batch_matmul(h_rt, gate["rt"]) +
                                    F.batch_matmul(h[i], gate["r"]), h[i].data.shape)
                    new_h.append(encdec.activation(h_l))
                    #
                    edge_lh = pydot.Edge(node_dic["%d_%d" % (src_len-cur_layer+1, i)],
                                         node_dic["%d_%d" % (src_len-cur_layer+2, i)],
                                         # label="")
                                         label="%1.2f" % gate["lh"].data)
                    edge_lh.set_color(ret_color(gate["lh"].data))
                    graph.add_edge(edge_lh)

                    edge_lt = pydot.Edge(node_dic["%d_%d" % (src_len-cur_layer+1, i)],
                                         node_dic["%d_%d/%d" % (src_len-cur_layer+2, i-1, i)],
                                         # label="")
                                         label="%1.2f" % gate["lt"].data)
                    edge_lt.set_color(ret_color(gate["lt"].data))
                    graph.add_edge(edge_lt)

                    edge_rh = pydot.Edge(node_dic["%d_%d" % (src_len-cur_layer+1, i+1)],
                                         node_dic["%d_%d" % (src_len-cur_layer+2, i)],
                                         # label="")
                                         label="%1.2f" % gate["rh"].data)
                    edge_rh.set_color(ret_color(gate["rh"].data))
                    graph.add_edge(edge_rh)

                    edge_rt = pydot.Edge(node_dic["%d_%d" % (src_len-cur_layer+1, i+1)],
                                         node_dic["%d_%d/%d" % (src_len-cur_layer+2, i, i+1)],
                                         # label="")
                                         label="%1.2f" % gate["rt"].data)
                    edge_rt.set_color(ret_color(gate["rt"].data))
                    graph.add_edge(edge_rt)

            new_h.append(dummy_node)
            h = new_h
            cur_layer -= 1

        h = h[1]
        if len(h) > 0:
            encdec.set_h(h[0])

    else:
        # encode
        h = []
        for l in xrange(src_len):
            x = XP.iarray([src_stoi(src_batch[k][l]) for k in xrange(batch_size)])
            h.append(encdec.make_h_0(x))
        cur_layer = src_len
        while cur_layer > 1:
            new_h = []
            new_h.append(dummy_node)
            for i in xrange(1, cur_layer):
                h_lt, h_rt, gate = encdec.encode(h[i-1], h[i], h[i+1])
                if i == 1:
                    # h_l = encdec.activation(F.reshape(F.batch_matmul(h_lt, gate["lt"]) +
                                                      # F.batch_matmul(h[i], gate["l"]), h[i].data.shape))
                    h_r = encdec.activation(F.reshape(F.batch_matmul(h_rt, gate["rt"]) +
                                                      F.batch_matmul(h[i], gate["r"]), h[i].data.shape))
                else:
                    h_l = h_r + encdec.activation(F.reshape(F.batch_matmul(h_lt, gate["lt"]) +
                                                            F.batch_matmul(h[i], gate["l"]), h[i].data.shape))
                    h_r = encdec.activation(F.reshape(F.batch_matmul(h_rt, gate["rt"]) +
                                                      F.batch_matmul(h[i], gate["r"]), h[i].data.shape))
                    new_h.append(h_l)
            new_h.append(dummy_node)
            h = new_h
            cur_layer -= 1

        h = h[1]
        if len(h) > 0:
            encdec.set_h(h[0])

    # decode
    t = XP.iarray([trg_stoi('<s>') for _ in xrange(batch_size)])
    hyp_batch = [[] for _ in xrange(batch_size)]

    while len(hyp_batch[0]) < generation_limit:
        y = encdec.decode(t)
        output = cuda.to_cpu(y.data.argmax(1))
        t = XP.iarray(output)
        for k in xrange(batch_size):
            hyp_batch[k].append(trg_itos(output[k]))
        if all(hyp_batch[k][-1] == '</s>' for k in xrange(batch_size)):
            break

    if write_graph:
        return hyp_batch, graph
    else:
        return hyp_batch
