# -*- coding: utf-8 -*-
from __future__ import print_function, division

from chainer import Chain

from networks.encoder import grConv, grConvNormalizeInputgate
from networks.decoder import LSTM, ResidualLSTM


class EncoderFactory(object):
    def __init__():
        pass

    @staticmethod
    def factory_method(encoder_type):
        if encoder_type == "grConv":
            return grConv
        if encoder_type == "grConvNormalizeInputgate":
            return grConvNormalizeInputgate


class DecoderFactory(object):
    def __init__():
        pass

    @staticmethod
    def factory_method(decoder_type):
        if decoder_type == "LSTM":
            return LSTM
        elif decoder_type == "ResidualLSTM":
            return ResidualLSTM


class EncoderDecoder(Chain):
    def __init__(self, src_vocab_size, trg_vocab_size, src_embed_size, trg_embed_size, hidden_size, encoder_type="grConv", decoder_type="ResidualLSTM"):
        super(EncoderDecoder, self).__init__(
            encoder=EncoderFactory.factory_method(encoder_type)(src_vocab_size, src_embed_size, hidden_size),
            decoder=DecoderFactory.factory_method(decoder_type)(trg_vocab_size, trg_embed_size, hidden_size)
        )
        self.src_vocab_size = src_vocab_size
        self.trg_vocab_size = trg_vocab_size
        self.src_embed_size = src_embed_size
        self.trg_embed_size = trg_embed_size
        self.hidden_size = hidden_size
        self.encoder_type = encoder_type
        self.decoder_type = decoder_type

    def reset(self, batch_size):
        self.zerograds()
        self.encoder.reset(batch_size)
        self.decoder.reset(batch_size)

    def encode(self, x_batch, vocab):
        return self.encoder(x_batch, vocab)

    def decode(self, y_batch, vocab, generation_limit=30, is_training=True):
        return self.decoder(y_batch, vocab, generation_limit, is_training)

    def forward(self, src_batch, trg_batch, src_vocab, trg_vocab, generation_limit=30, is_training=True):
        batch_size = len(src_batch)
        self.reset(batch_size)
        self.encode(src_batch, src_vocab)
        h_inter = self.encoder.get_h()
        self.decoder.set_h(h_inter)
        return self.decode(trg_batch, trg_vocab, generation_limit, is_training)

    def save_spec(self, filename):
        with open(filename, 'w') as fp:
            print(self.src_vocab_size, file=fp)
            print(self.trg_vocab_size, file=fp)
            print(self.src_embed_size, file=fp)
            print(self.trg_embed_size, file=fp)
            print(self.hidden_size, file=fp)
            print(self.encoder_type, file=fp)
            print(self.decoder_type, file=fp)

    @staticmethod
    def load_spec(filename):
        with open(filename) as fp:
            src_vocab_size = int(next(fp))
            trg_vocab_size = int(next(fp))
            src_embed_size = int(next(fp))
            trg_embed_size = int(next(fp))
            hidden_size = int(next(fp))
            try:
                encoder_type = str(next(fp).strip())
            except:
                encoder_type = "grConv"
            try:
                decoder_type = str(next(fp).strip())
            except:
                decoder_type = "LSTM"

            return EncoderDecoder(src_vocab_size,
                                  trg_vocab_size,
                                  src_embed_size,
                                  trg_embed_size,
                                  hidden_size,
                                  encoder_type=encoder_type,
                                  decoder_type=decoder_type)
