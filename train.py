from __future__ import print_function, division

import os
import math
import configparser
from subprocess import check_output
from argparse import ArgumentParser

from chainer import optimizer, optimizers, serializers

import util.generators as gens
from util.calc_bleu import calc_bleu_sentence
from util.functions import trace, fill_batch3 as fill_batch
from util.progress_bar import progress_bar
from util.vocabulary import Vocabulary
from util.XP import XP

from networks.encoder_decoder import EncoderDecoder


def parse_config(config_file):
    parser_config = configparser.ConfigParser()
    parser_config.read(config_file)

    config = parser_config["settings"]

    settings = {}
    # model parameters
    settings["vocab"] = config.getint("vocab")
    settings["embed"] = config.getint("embed")
    settings["hidden"] = config.getint("hidden")
    settings["encoder"] = config["encoder"]
    settings["decoder"] = config["decoder"]
    # experimental setups
    settings["optimizer"] = config["optimizer"]
    settings["weight_decay"] = float(config["weight_decay"])
    settings["gradient_clipping"] = int(config["gradient_clipping"])
    settings["minibatch"] = config.getint("minibatch")
    settings["generation_limit"] = config.getint("generation_limit")
    settings["save_model"] = config.getint("save_model")
    settings["save_opt"] = config.getint("save_opt")

    data = parser_config["data"]

    return settings, data


def parse_args():
    p = ArgumentParser()

    p.add_argument('model', help='[in/out] model file')
    p.add_argument('--epoch', type=int, help='# of epoch')
    p.add_argument('--resume', default=None, type=str, help='resume training from this model')
    p.add_argument('--use-gpu', action='store_true', default=False, help='use GPU calculation')
    p.add_argument('--gpu-device', type=int, help='GPU device ID to be used (default: %(default)d)')
    p.add_argument('--config-file', default="config/train.ini", metavar='STR', type=str, help='config_file')
    p.add_argument('--log-dir', default="./", metavar='STR', type=str, help='log directory')

    args = p.parse_args()

    return args


def train(args, settings, data):
    if args.resume is None:
        trace('making vocabularies ...')
        src_vocab = Vocabulary.new(gens.word_list(data["train_src"]), settings["vocab"])
        trg_vocab = Vocabulary.new(gens.word_list(data["train_trg"]), settings["vocab"])
        src_vocab_size = src_vocab.__len__()
        trg_vocab_size = trg_vocab.__len__()
        trace('src_vocab_size: %d' % src_vocab_size)
        trace('trg_vocab_size: %d' % trg_vocab_size)
        trace('making model ...')
        encdec = EncoderDecoder(src_vocab_size, trg_vocab_size,
                                settings["embed"], settings["embed"], settings["hidden"],
                                encoder_type=settings["encoder"], decoder_type=settings["decoder"])
        resume_epoch = 0
    else:
        trace('loading model ...')
        src_vocab = Vocabulary.load(args.resume + '.srcvocab')
        trg_vocab = Vocabulary.load(args.resume + '.trgvocab')
        encdec = EncoderDecoder.load_spec(args.resume + '.spec')
        serializers.load_hdf5(args.resume + '.weights', encdec)
        resume_epoch = int(args.resume.split(".")[-1])

    if args.use_gpu:
        encdec.to_gpu()

    with open(args.log_dir + "./loss.log", "w") as fp:
        print("epoch,total_loss", file=fp)
    with open(args.log_dir + "./dev_score.log", "w") as fp:
        print("epoch,bleu_all,bleu1,bleu2,bleu3,bleu4", file=fp)
#    with open(args.log_dir + "./dev_loss.log", "w") as fp:
#        print("epoch,total_loss", file=fp)

    data_size = int(check_output(["wc", "-l", data["train_src"]]).split()[0])
    p_bar = progress_bar(data_size)
    for epoch in xrange(resume_epoch, args.epoch):
        trace('epoch %d/%d: ' % (epoch + 1, args.epoch))
        p_bar.reset()
        trained = 0
        # make minibatch
        gen1 = gens.word_list(data["train_src"])
        gen2 = gens.word_list(data["train_trg"])
        gen3 = gens.batch_samelength(gens.sorted_parallel(gen1, gen2, 100 * settings["minibatch"]), settings["minibatch"])

        # set optimizer
        if settings["optimizer"] == "Adam":
            opt = optimizers.Adam()
        elif settings["optimizer"] == "AdaDelta":
            opt = optimizers.AdaDelta()
        elif settings["optimizer"] == "AdaGrad":
            opt = optimizers.AdaGrad()
        elif settings["optimizer"] == "RMSprop":
            opt = optimizers.RMSprop()
        elif settings["optimizer"] == "RMSpropGraves":
            opt = optimizers.RMSpropGraves()
        elif settings["optimizer"] == "SGD":
            learning_rate = 1.0 * (0.5 ** (epoch // 5))
            opt = optimizers.SGD(lr=learning_rate)

        opt.setup(encdec)
        opt.add_hook(optimizer.GradientClipping(settings["gradient_clipping"]))
        opt.add_hook(optimizer.WeightDecay(settings["weight_decay"]))

        trace("setup optimizer: {}, gradient_clipping: {}, weight_decay:{}".format(
            settings["optimizer"], settings["gradient_clipping"], settings["weight_decay"])
        )

        # load optimizer's parameters
        if args.resume is not None:
            serializers.load_hdf5(args.resume + '.optimizer', opt)

        # training
        prog_log = open(args.log_dir + "progress.log", "w")
        total_loss = 0.0
        for src_batch, trg_batch in gen3:
            # src_batch = fill_batch(src_batch)
            trg_batch = fill_batch(trg_batch)
            K = len(src_batch)
            hyp_batch, loss = encdec.forward(src_batch, trg_batch, src_vocab, trg_vocab, is_training=True)
            loss.backward()
            total_loss += loss.data
            opt.update()

            for k in xrange(K):
                try:
                    trace('epoch %3d/%3d, sample %8d' % (epoch + 1, args.epoch, trained + k + 1), out_file=prog_log)
                    trace('  src = ' + str(' '.join([x if x != '</s>' else '*' for x in src_batch[k]])), out_file=prog_log)
                    trace('  trg = ' + str(' '.join([x if x != '</s>' else '*' for x in trg_batch[k]])), out_file=prog_log)
                    trace('  hyp = ' + str(' '.join([x if x != '</s>' else '*' for x in hyp_batch[k]])), out_file=prog_log)
                except:
                    pass

            trained += K
            p_bar.update(trained)

        with open(args.log_dir + "./loss.log", "a") as fp:
            print("%d,%f" % (epoch+1, total_loss), file=fp)

        if ((epoch+1) % settings["save_model"]) == 0:
            trace('saving model ...')
            prefix = args.model + '.%05.d' % (epoch + 1)
            src_vocab.save(prefix + '.srcvocab')
            trg_vocab.save(prefix + '.trgvocab')
            encdec.save_spec(prefix + '.spec')
            serializers.save_hdf5(prefix + '.weights', encdec)

        if ((epoch+1) % settings["save_opt"]) == 0:
            serializers.save_hdf5(prefix + '.optimizer', opt)

        if data["dev_src"] is not None:
            trace('test on validation set ...')
            score = validate(data["dev_src"], data["dev_trg"], src_vocab, trg_vocab, encdec, settings["generation_limit"])
            trace("BLEU: %.2f,%.2f,%.2f,%.2f,%.2f" % (score[0], score[1], score[2], score[3], score[4]))
            with open(args.log_dir + "./dev_score.log", "a") as fp:
                print("%d,%.2f,%.2f,%.2f,%.2f,%.2f" % (epoch+1, score[0], score[1], score[2], score[3], score[4]), file=fp)

            """
            # validation using loss is not good.
            total_loss = 0.0
            gen1 = gens.word_list(data["dev_src"])
            gen2 = gens.word_list(data["dev_trg"])
            gen3 = gens.batch_samelength(gens.sorted_parallel(gen1, gen2, 10 * settings["minibatch"]), settings["minibatch"])
            for src_batch, trg_batch in gen3:
                trg_batch = fill_batch(trg_batch)
                _, loss = forward(src_batch, trg_batch, src_vocab, trg_vocab, encdec)
                total_loss += loss.data

            with open(args.log_dir + "./dev_loss.log", "a") as fp:
                print("%d,%f" % (epoch+1, total_loss), file=fp)
            """

    if data["test_src"] is not None:
        trace('test on test set ...')
        score = validate(data["test_src"], data["test_trg"], src_vocab, trg_vocab, encdec, settings["generation_limit"])
        trace("BLEU: %.2f,%.2f,%.2f,%.2f,%.2f" % (score[0], score[1], score[2], score[3], score[4]))
        with open(args.log_dir + "./test_score", "w") as fp:
            print("%.2f,%.2f,%.2f,%.2f,%.2f" % (score[0], score[1], score[2], score[3], score[4]), file=fp)

    trace('finished.')


def validate(dev_src, dev_trg, src_vocab, trg_vocab, encdec, generation_limit):
    gen1 = gens.word_list(dev_src)
    gen2 = gens.word_list(dev_trg)
    gen3 = gens.batch_samelength(gens.sorted_parallel(gen1, gen2, 100), 10)

    generated = 0
    sum_bleu = [0.0, 0.0, 0.0, 0.0]
    trg_corpus_len = 0
    hyp_corpus_len = 0
    BP = 1.0
    for src_batch, trg_batch in gen3:
        hyp_batch = encdec.forward(src_batch, trg_batch, src_vocab, trg_vocab, is_training=False)

        for src, trg, hyp in zip(src_batch, trg_batch, hyp_batch):
            trg_corpus_len += len(trg)
            hyp_corpus_len += len(hyp)
            hyp.append('</s>')
            hyp = str(' '.join(hyp[:hyp.index('</s>')]))
            trg = str(' '.join(trg))
            src = str(' '.join(src))
            try:
                _, p_list = calc_bleu_sentence(trg, hyp, max_ngram=4)
                sum_bleu = [s + n for (s, n) in zip(sum_bleu, p_list)]
            except Exception as e:
                trace("can't calc bleu !\ntrg:{}\nhyp:{}".format(trg, hyp))
                trace('=== Error ===')
                trace('type:' + str(type(e)))
                trace('args:' + str(e.args))
                trace('message:' + e.message)

            generated += 1

    if hyp_corpus_len < trg_corpus_len:
        BP = math.exp(1 - trg_corpus_len/hyp_corpus_len)

    return (math.fsum(sum_bleu)/4.0 * BP * 100.0 / generated,
            sum_bleu[0]*100.0/generated, sum_bleu[1]*100.0/generated,
            sum_bleu[2]*100.0/generated, sum_bleu[3]*100.0/generated,
            )


def main():
    args = parse_args()
    XP.set_library(args)

    if not os.path.exists(args.log_dir):
        os.makedirs(args.log_dir)
    settings, data = parse_config(args.config_file)

    train(args, settings, data)


if __name__ == '__main__':
    main()
