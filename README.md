#Gated Recursive Convolution

## Requirements
`chainer` => 1.8.0  
`pydot`  
`nltk`  
`chainer requirements` (h5py, cython, etc.)

## References
[On the Properties of Neural Machine tranlation: Encoder-Decoder Approaches](http://arxiv.org/abs/1409.1259) [Cho+, 2014]


