from __future__ import print_function, division

import os
import math
import configparser
from argparse import ArgumentParser

from chainer import serializers
import util.generators as gens
from util.calc_bleu import calc_bleu_sentence
from util.functions import trace
from util.vocabulary import Vocabulary
from util.XP import XP

from net import grConvEncoderDecoder as EncoderDecoder, forward_test as forward
# from stacked_encdec import EncoderDecoder, forward
# from encdec import EncoderDecoder, forward


def parse_config(config_file):
    parser_config = configparser.ConfigParser()
    parser_config.read(config_file)

    config = parser_config["settings"]

    settings = {}
    settings["generation_limit"] = config.getint("generation_limit")
    settings["encoder"] = config["encoder"]
    try:
        settings["minibatch"] = config.getint("minibatch")
    except:
        settings["minibatch"] = 1

    data = parser_config["data"]

    return settings, data


def parse_args():
    p = ArgumentParser()

    p.add_argument('output', help='hypothesis')
    p.add_argument('model', help='model file')
    p.add_argument('--use-gpu', action='store_true', default=False, help='use GPU calculation')
    p.add_argument('--gpu-device', type=int, help='GPU device ID to be used')
    p.add_argument('--write-graph', action='store_true', default=False, help='write graphs')
    p.add_argument('--graph-dir',  type=str, help='graph directory')
    p.add_argument('--config-file', default=None, type=str)
    args = p.parse_args()

    return args


def test(args, settings, data):
    trace('loading model ...')
    src_vocab = Vocabulary.load(args.model + '.srcvocab')
    trg_vocab = Vocabulary.load(args.model + '.trgvocab')
    encdec = EncoderDecoder.load_spec(args.model + '.spec')
    if args.use_gpu:
        encdec.to_gpu()
    serializers.load_hdf5(args.model + '.weights', encdec)

    gen1 = gens.word_list(data["test_src"])
    gen2 = gens.word_list(data["test_trg"])
    gen3 = gens.batch_samelength(gens.sorted_parallel(gen1, gen2, 100 * settings["minibatch"]), settings["minibatch"])

    trace('generating translation ...')
    generated = 0
    sum_bleu = [0.0, 0.0, 0.0, 0.0]
    trg_corpus_len = 0
    hyp_corpus_len = 0
    BP = 1.0
    with open(args.output, 'w') as fp:
        # write settings on result file
        print("model:{model}\ngeneration_limit:{gen_lim}\ntest_source:{test_src}\ntest_target:{test_trg}\n\
              ".format(model=args.model,
                       gen_lim=settings["generation_limit"],
                       test_src=data["test_src"],
                       test_trg=data["test_trg"]), file=fp)
        for src_batch, trg_batch in gen3:
            trace('sample %5d ...' % (generated + 1))

            if args.write_graph:
                hyp_batch, graph = forward(src_batch, None, src_vocab, trg_vocab, encdec, settings["generation_limit"], args.write_graph)
                graph.write_png(args.graph_dir+'/test_%04.d.png' % (generated+1), prog="dot")
            else:
                hyp_batch = forward(src_batch, None, src_vocab, trg_vocab, encdec, settings["generation_limit"], args.write_graph)

            for src, trg, hyp in zip(src_batch, trg_batch, hyp_batch):
                trg_corpus_len += len(trg)
                hyp_corpus_len += len(hyp)
                src = ' '.join(src)
                trg = ' '.join(trg)
                hyp.append('</s>')
                hyp = ' '.join(hyp[:hyp.index('</s>')])
                print('num:{}'.format(generated+1), file=fp)
                print('src:{}'.format(src), file=fp)
                print('trg:{}'.format(trg), file=fp)
                print('hyp:{}'.format(hyp), file=fp)
                try:
                    p_all, p_list = calc_bleu_sentence(trg, hyp, 4)
                except:
                    p_all, p_list = 0, [0, 0, 0, 0]
                print('BLEU:%.2f/%.2f/%.2f/%.2f/%.2f\
                      ' % (p_all, p_list[0], p_list[1], p_list[2], p_list[3]), file=fp)
                generated += 1
                sum_bleu = [s + n for (s, n) in zip(sum_bleu, p_list)]

        if hyp_corpus_len < trg_corpus_len:
            BP = math.exp(1 - trg_corpus_len/hyp_corpus_len)

        print('\nBLEU CORPUS:%.2f/%.2f/%.2f/%.2f/%.2f' % (
              math.fsum(sum_bleu)/4.0 * BP * 100.0 / generated,
              sum_bleu[0]*100.0/generated, sum_bleu[1]*100.0/generated,
              sum_bleu[2]*100.0/generated, sum_bleu[3]*100.0/generated,
              ), file=fp)

    trace('finished.')


def main():
    args = parse_args()
    XP.set_library(args)

    settings, data = parse_config(args.config_file)
    if args.write_graph:
        settings["minibatch"] = 1
        if not os.path.exists(args.graph_dir):
            os.makedirs(args.graph_dir)

    test(args, settings, data)

if __name__ == '__main__':
    main()
