from __future__ import division

import sys
import time


class progress_bar(object):
    def __init__(self, max_val, bar_length=30, bar_char="#"):
        self.start_time = time.time()
        self.max_val = max_val
        self.bar_length = bar_length
        self.bar_char = bar_char

    def reset(self):
        self.start_time = time.time()

    def update(self, cur_val):
        percent = round(cur_val / self.max_val, 4)
        bar_prog = int(round(percent * self.bar_length))
        bar = self.bar_char * bar_prog + ' ' * (self.bar_length - bar_prog)
        elapsed_time = time.time() - self.start_time
        try:
            remain_time = elapsed_time / percent - elapsed_time
        except:
            remain_time = 0.0
        sys.stderr.write("\r[{}] {:.1f}% {}/{} elapsed:{:.1f}sec. remain:{:.1f}sec.".format(
            bar, percent*100, cur_val, self.max_val, elapsed_time, remain_time)
        )
        sys.stderr.flush()
        if percent == 1.0:
            sys.stderr.write("\n")

if __name__ == '__main__':
    max_num = 2000
    p = progress_bar(max_num)
    for i in xrange(1, max_num+1):
        p.update(i)
        time.sleep(0.001)
    print "finished."
