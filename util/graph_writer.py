import pydot


def ret_color(value):
    if value > 0.8:
        return "red"
    # elif value > 0.2:
        # return "royalblue"
    elif value > 0.10:
        return "gray10"
    else:
        return "gray90"


def set_initial_graph(sentence, stoi):
    src_len = len(sentence)
    node_dic = {}
    graph = pydot.Dot(graph_type='graph', ranksep="0.3")

    for word in sentence:
        if stoi(word) != 0:
            node_dic[word] = pydot.Node("%s" % word, label='%s' % word)
        else:
            node_name = "OOV%s" % word
            node_dic[word] = pydot.Node(node_name)
        graph.add_node(node_dic[word])

    num_node = src_len + 1
    for layer in xrange(1, src_len + 1):
        for number in xrange(1, num_node):
            node_name = "%d_%d" % (layer, number)
            node_dic[node_name] = pydot.Node(node_name, fixedsize="true", width="0.25", height="0.25", label="")
            graph.add_node(node_dic[node_name])
            if number != num_node - 1:
                inter_node_name = "%d_%d/%d" % (layer, number, number+1)
                node_dic[inter_node_name] = pydot.Node(inter_node_name, style="filled", fillcolor="gray50",
                                                       fixedsize="true", width="0.1", height="0.1", label="")
                graph.add_node(node_dic[inter_node_name])
        num_node -= 1

    for k in xrange(src_len):
        graph.add_edge(pydot.Edge(node_dic[sentence[k]], node_dic["%d_%d" % (1, k+1)]))
    """
    num_node = src_len + 1
    for layer in xrange(1, src_len + 1):
        for number in xrange(1, num_node):
            if number != num_node - 1:
                left_node_name = "%d_%d" % (layer, number)
                right_node_name = "%d_%d" % (layer, number+1)
                inter_node_name = "%d_%d/%d" % (layer, number, number+1)
                graph.add_edge(pydot.Edge(node_dic[left_node_name], node_dic[inter_node_name], color="gray40", style="dashed"))
                graph.add_edge(pydot.Edge(node_dic[right_node_name], node_dic[inter_node_name], color="gray40", style="dashed"))
        num_node -= 1
    """
    return graph, node_dic
