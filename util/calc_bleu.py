from __future__ import division
from nltk.translate.bleu_score import modified_precision
from math import exp, log


def calc_bleu_sentence(ref, hyp, max_ngram=4):
    ref = ref.split()
    hyp = hyp.split()

    p_list = []
    for n in xrange(1, max_ngram+1):
        p_list.append(float(modified_precision([ref], hyp, n)))

    nonzero = max_ngram
    log_sum = 0.0
    for p in p_list:
        try:
            log_sum += log(p)
        except ValueError:
            nonzero -= 1

    if log_sum != 0:
        log_sum /= nonzero
        p_all = exp(log_sum)
    else:
        p_all = 0

    return p_all, p_list

if __name__ == '__main__':
    ref = str("I am a man and work on the google")
    # hyp = ""
    hyp = str("I is you love who")
    p_all, p_list = calc_bleu_sentence(ref, hyp, max_ngram=4)
    print p_all
