from __future__ import print_function, division


def batch(generator, batch_size):
    batch = []
    is_tuple = False
    for l in generator:
        is_tuple = isinstance(l, tuple)
        batch.append(l)
        if len(batch) == batch_size:
            yield tuple(list(x) for x in zip(*batch)) if is_tuple else batch
            batch = []
    if batch:
        yield tuple(list(x) for x in zip(*batch)) if is_tuple else batch


def batch_samelength_mono(generator, batch_size):
    batch = []
    is_tuple = False
    len_list = []
    for l in generator:
        is_tuple = isinstance(l, tuple)
        len_list.append(len(l))
        if (len(batch) == batch_size) or (len_list[0] != len_list[-1]):
            del len_list[:len(batch)]
            yield tuple(list(x) for x in zip(*batch)) if is_tuple else batch
            batch = []
        batch.append(l)

    if batch:
        yield tuple(list(x) for x in zip(*batch)) if is_tuple else batch


def batch_samelength(parallel_generator, batch_size):
    batch = []
    is_tuple = False
    len_list = []
    for l in parallel_generator:
        is_tuple = isinstance(l, tuple)
        len_list.append(len(l[0]))
        if (len(batch) == batch_size) or (len_list[0] != len_list[-1]):
            del len_list[:len(batch)]
            yield tuple(list(x) for x in zip(*batch)) if is_tuple else batch
            batch = []
        batch.append(l)
    if batch:
        yield tuple(list(x) for x in zip(*batch)) if is_tuple else batch


def sorted_parallel(generator_src, generator_trg, pooling, order=0):
    gen1 = batch(generator_src, pooling)
    gen2 = batch(generator_trg, pooling)
    for batch1, batch2 in zip(gen1, gen2):
        # yield from sorted(zip(batch1, batch2), key=lambda x: len(x[0]))
        for x in sorted(zip(batch1, batch2), key=lambda x: len(x[order])):
        # for x in zip(batch1, batch2):
            yield x


def word_list(filename):
    with open(filename) as fp:
        for l in fp:
            yield l.split()


def letter_list(filename):
    with open(filename) as fp:
        for l in fp:
            yield list(''.join(l.split()))
