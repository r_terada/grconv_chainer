from __future__ import print_function, unicode_literals, division

import sys
import datetime


def trace(text, out_file=sys.stderr):
    print(datetime.datetime.now(), '...', text, file=out_file)
    sys.stderr.flush()


def fill_batch(batch, token='</s>'):
    max_len = max(len(x) for x in batch)
    return [x + [token] * (max_len - len(x) + 1) for x in batch]


def fill_batch2(batch, start_token='<s>', end_token='</s>'):
    max_len = max(len(x) for x in batch)
    return [[start_token] + x + [end_token] * (max_len - len(x) + 1) for x in batch]


def fill_batch3(batch, start_token='<s>', end_token='</s>', ignore_label='<ignore>'):
    max_len = max(len(x) for x in batch)
    return [[start_token] + x + [end_token] + [ignore_label] * (max_len - len(x)) for x in batch]


if __name__ == '__main__':
    trace("test")
    x = [["I", "have", "a", "pen"], ["the", "cat"]]
    trace(x)
    trace(fill_batch(x))
    trace(fill_batch2(x))
    trace(fill_batch3(x))
