from __future__ import print_function, division

import os

if __name__ == '__main__':
    train_src = "/almond/chu/Paraphrase_BLE_SMT/SMT/data/zh/train.zh"
    train_trg = "/almond/chu/Paraphrase_BLE_SMT/SMT/data/en/train.en"

    max_len = 10
    num_line = 30000

    output_dir = "maxlen" + str(max_len) + "_numline" + str(num_line) + "_uniq"
    os.makedirs(output_dir)

    src_output_train = output_dir+"/train.zh"
    trg_output_train = output_dir+"/train.en"

    trg_output_dev = output_dir+"/dev.en"
    src_output_dev = output_dir+"/dev.zh"

    src_output_test = output_dir+"/test.zh"
    trg_output_test = output_dir+"/test.en"

    dupli_checker = []
    with open(train_src, "r") as src_fp:
        with open(train_trg, "r") as trg_fp:
            with open(src_output_train, "w") as src_train:
                with open(trg_output_train, "w") as trg_train:
                    with open(src_output_dev, "w") as src_dev:
                        with open(trg_output_dev, "w") as trg_dev:
                            with open(src_output_test, "w") as src_test:
                                with open(trg_output_test, "w") as trg_test:
                                    i = 0
                                    j = 0
                                    k = 0
                                    train_done = False
                                    dev_done = False
                                    test_done = False
                                    for src_line, trg_line in zip(src_fp, trg_fp):
                                        if len(src_line.strip().split()) > max_len:
                                            continue
                                        if len(trg_line.strip().split()) > max_len:
                                            continue
                                        if src_line in dupli_checker:
                                            continue

                                        if (i >= num_line) and (train_done is False):
                                            train_done = True
                                        if (train_done is True) and (j >= 1000) and (dev_done is False):
                                            dev_done = True
                                        if (dev_done is True) and (k >= 1000) and (test_done is False):
                                            test_done = True

                                        if train_done is False:
                                            i += 1
                                            print(src_line.strip(), file=src_train)
                                            print(trg_line.strip(), file=trg_train)
                                            dupli_checker.append(src_line)
                                        elif dev_done is False and train_done is True:
                                            j += 1
                                            print(src_line.strip(), file=src_dev)
                                            print(trg_line.strip(), file=trg_dev)
                                            dupli_checker.append(src_line)
                                        elif test_done is False and dev_done is True and train_done is True:
                                            k += 1
                                            print(src_line.strip(), file=src_test)
                                            print(trg_line.strip(), file=trg_test)
                                            dupli_checker.append(src_line)
                                        elif test_done is True and dev_done is True and train_done is True:
                                            break

                                        else:
                                            print("error!")

    print("train_line_num = " + str(i) + "max_word_num = " + str(max_len))
    print("dev_line_num = " + str(j) + "max_word_num = " + str(max_len))
    print("test_line_num = " + str(j) + "max_word_num = " + str(max_len))


    """
    with open(train_src) as src_fp:
        len_dic = {}
        for i, line in enumerate(src_fp):
            len_dic[i] = len(line.split())

    with open(train_src) as src_fp:
        with open(train_trg) as trg_fp:
            with open(src_output_train, "w") as src_out:
                with open(trg_output_train, "w") as trg_out:
                    all_src = src_fp.read().split("\n")
                    all_trg = trg_fp.read().split("\n")
                    for k, v in sorted(len_dic.items(), key=lambda x: x[1]):
                        print(v)
                        print(all_src[k], file=src_out)
                        print(all_trg[k], file=trg_out)
    """
