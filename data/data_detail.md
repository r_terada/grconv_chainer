## maxlen10_numline30000_uniq:
 max sentence length = 10  
 -train  
   vocab_src: 17679  
   vocab_trg: 14222  
   # of pairs: 30000  
   no same lines  

 -dev  
   voacb_src: 2160 (OOV: 328)  
   voacb_trg: 2170 (OOV: 227)  
   # of pairs: 1000  

 -test  
   vocab_src: 2189(OOV: 314)  
   vocab_trg: 2097(OOV: 222)  
   # of pairs: 1000  

## maxlen20_numline50000_uniq:
 max sentence length = 20  
 -train  
   vocab_src: 28451  
   vocab_trg: 22234  
   # of pairs: 50000  
   no same lines  
  
 -dev  
   voacb_src: 2784 (OOV: 272)  
   voacb_trg: 2769 (OOV: 224)  
   # of pairs: 1000  

 -test  
   vocab_src: 2902 (OOV: 402)  
   vocab_trg: 2711 (OOV: 95)  
   # of pairs: 1000  

## maxlen30_full_uniq:
 max sentence length = 30  
 -train  
   vocab_src: 163582  
   vocab_trg: 89736  
   # of pairs: 570896  

 -dev  
   voacb_src: 4307 (OOV: 292)  
   voacb_trg: 4030 (OOV: 450)  
   # of pairs: 1000  
