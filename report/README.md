## 各ディレクトリの中身
   - dev_score.log  
     dev setでのBLEU score. 
   - loss.log  
     training loss  
   - result.~  
     test setでの各文の翻訳結果, BLEU score.  
   - settings  
     実験のconfig file  
   - test_score
     test setでのBLEU score. (全体/1-gram/2-gram/3-gram/4-gram)  