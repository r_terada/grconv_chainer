START_DATE=$(date +%Y%m%d_%H%M)
PYTHON="/usr/bin/python"
GPU="--use-gpu --gpu-device 1"
# PYTHON="nice -19 /share/usr-x86_64/bin/python"
# GPU=""

CONFIG=$1
EPOCH=20

LOG_DIR=log_experiment/${START_DATE}/
MODEL_DIR=/windroot/`whoami`/grConv_model/${START_DATE}/

# GRAPH="--write-graph --graph-dir /windroot/`whoami`/grConv_graph/${START_DATE}/"
# mkdir /windroot/`whoami`/grConv_graph/${START_DATE}/
GRAPH=""

mkdir ${LOG_DIR}
mkdir ${MODEL_DIR}

cp ${CONFIG} log_experiment/${START_DATE}/settings

echo ${START_DATE}: >> note_experiment
echo \ \ ${CONFIG} >> note_experiment

# training
${PYTHON} train.py ${MODEL_DIR}/model --epoch ${EPOCH} --config-file ${CONFIG} --log-dir ${LOG_DIR} ${GPU}
# test
LATEST_MODEL=$(basename `ls ${MODEL_DIR}|tail -1` .weights)
${PYTHON} test.py ${LOG_DIR}/result.${LATEST_MODEL} ${MODEL_DIR}/${LATEST_MODEL} --config-file ${CONFIG} ${GPU} ${GRAPH}

echo "Finished!!"
